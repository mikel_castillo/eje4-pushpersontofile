/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package asynchronoustasks;

/**
 *
 * @author Casel
 */
/**
* Represents a task that inserts a number of people in a list.
*
* @author Eugenia Pérez Martínez
*/
public class AsynchronousTasks {
/**
* Entry point of the app.
*
* @param args the command line arguments
*/
    public static void main(String[] args) {
        int numberOfPeople = 20;
        Thread t1 = new Thread(new PushPeopleToFile(numberOfPeople));
        t1.start();
        Thread t2 = new Thread(new PushPeopleToList(numberOfPeople));
        t2.start();
        
//        made no secc
//        new PushPeopleToFile(numberOfPeople).run();
//        new PushPeopleToList(numberOfPeople).run();
    }
}
