/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package asynchronoustasks;

import java.util.Random;

/**
 *
 * @author Casel
 */

class Person {
//TODO: Declare the attributes of a person: age and dni
/**
* Class constructor.
*/
    int age=0;
    String dni="";
public Person() {
//Initialize attributes to random values.The age must be an integer
//between 0 and 100, and the dni a 8 characters alphanumeric string
    Random rand= new Random();
    
    age= rand.nextInt(100);
 //too letters in the adcdary \( -.-)/
    char letras[]={'a','b','c','d','e','f','g','h'};
    for(int i = 0;i<8;i++){
        dni+=letras[(rand.nextInt(8))];
    }
    dni+=rand.nextInt(9);
}

@Override
public String toString() {
    return ("Person info= DNI: "+this.dni+" Ages: "+this.age);
}
}

