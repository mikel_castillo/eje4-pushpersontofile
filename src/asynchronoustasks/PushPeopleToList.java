/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package asynchronoustasks;


// @author Casel

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
* Represents a task that inserts a number of people in a list.
*
* @author Eugenia Pérez Martínez
*/
class PushPeopleToList implements Runnable {
/**
* Number of people to insert.
*/
private int numberOfPeople;
/**
* Class constructor.
*
* @param numberOfPeople people to insert.
*/
public PushPeopleToList(int numberOfPeople) {
this.numberOfPeople = numberOfPeople;
}
@Override
public void run() {
System.out.println("Starting to push people to an ArrayList");
    Vector<Person> people=new Vector<Person>();
    for(int i=0;i<=numberOfPeople;i++){
         Person p = new Person();
        people.add(p);
        System.out.println("List Line: "+ p.toString());
    try {
        Thread.sleep(500);
    } catch (InterruptedException ex) {
        Logger.getLogger(PushPeopleToList.class.getName()).log(Level.SEVERE, null, ex);
    }
    }
System.out.println("Everyone is already in the ArrayList");
    }
}
