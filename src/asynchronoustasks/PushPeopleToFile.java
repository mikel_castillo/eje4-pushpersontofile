/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package asynchronoustasks;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
* Represents a task that inserts a number of people in a text file.
*
* @author Eugenia Pérez Martínez
*/
class PushPeopleToFile implements Runnable {
/**
* Number of people to insert.
*/
private int numberOfPeople;
/**
* Class constructor.
*
* @param numberOfPeople people to insert.
*/
public PushPeopleToFile(int numberOfPeople) {
this.numberOfPeople = numberOfPeople;
}
@Override
public void run() {
System.out.println("Starting to push people to a text file");
//TODO: Crea los objetos necesarios para escribir a fichero en Java.
//A continuación, dentro de un bucle con el mismo número de iteraciones
    //que personas a insertar, crea una espera
//de medio segundo, crea la persona y vuélcala a fichero
        FileWriter fichero = null;
        PrintWriter pw = null;
        try
        {
            fichero = new FileWriter("people.txt");
            pw = new PrintWriter(fichero);
            for(int i=0;i<=numberOfPeople;i++){
                Person p = new Person();
        pw.println(p);
        System.out.println("File Line: "+p.toString());
        Thread.sleep(500);
    }
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
System.out.println("Everyone is already in a text file");
}   
}

